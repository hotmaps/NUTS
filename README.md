# Creation of NUTS vector files at level 0, 1, 2 and 3

## Reading of data on NUTS

#### NUTS 0
Selection of regions with STAT_LEVL_ = 0 and export of selected area in csv, csvt and prj file.

#### NUTS 1
Selection of regions with STAT_LEVL_ = 1 and export of selected area in csv, csvt and prj file.

#### NUTS 2
Selection of regions with STAT_LEVL_ = 2 and export of selected area in csv, csvt and prj file.

#### NUTS 3

Selection of regions with STAT_LEVL_ = 3 and export of selected area in csv, csvt and prj file.


## License


[Licensing EuroGeographics European geospatial data](https://eurogeographics.org/products-and-services/licensing/)

[Administrative units  Statistical units](https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units)